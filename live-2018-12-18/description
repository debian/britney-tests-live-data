Live data from 2018-12-18: hdf5/python3-defaults transition

This test has the live data from 2018-12-18, which has the conclusion of the
entangled hdf5 and python3-defaults transitions.

While searching for a way to get britney to migrate the packages involved in
these transitions, mdds and its rdeps libixion and liborcus where not
candidates yet (they were recent uploads). In that situation, the autohinter
is able to create a hint that only breaks python3-ixion and python3-orcus
(which both have no rdeps). During the search for a solution, mdds became a
candidate. Once that happened, the autohinter didn't find a working set of
packages. A 'hint' hint on hdf5 and python3-defaults also fails with a lot of
uninstallable packages. Using a 'hint' hint with the set of packages from the
autohinter mentioned above, britney was able to migrate everything.

The goal of this test is to have a testcase to improve both the autohinter and
the 'hint' hint code. It would be nice of both were able to find a working set
of packages.

In the base test, the age-policy-dates file is filtered to only include dates
for the sources that were candidates at that time. Other state date (bugs,
piuparts, debci) is missing to speed up the run. All this data is available in
a variant.

The variants for this testcase are in the directory variants-available. This
allow easy enable/disable of them using symlinks in variants, to deal with the
long runtime of this test.

Available variants:

- hint-hint: this variant has a 'hint' hint with hdf5 and python3-defaults
- big-hint: this variant has the (incomplete) set of packages from the
  autohinter as a 'hint' hint, which makes the migration succeed
- force-hint: this variant has hdf5 and python3-defaults forced in by a
  force-hint, to let britney try and clean it up. The result is the same as in
  the hint-hint variant, except that it isn't rolled back, so it's easy to
  compare the result with the result of other variant
- block-mdds: this variant has a block for mdds, creating the situation in
  which the auto-hinter almost finds a solution
- full-state: this variant has the full state info (bugs, piuparts, debci)



